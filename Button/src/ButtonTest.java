import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class ButtonTest extends JFrame implements ActionListener {

	private JButton button = new JButton("Reverse");
	private JTextField txtName = new JTextField("ENTER NUMBER TO REVERSE");
	
	public ButtonTest() {
		button.addActionListener(this);
		add(txtName, BorderLayout.NORTH);
		add(button, BorderLayout.SOUTH);
		setVisible(true);
		setSize(500,500);
	}
	
	public void actionPerformed(ActionEvent e) {
		txtName.setText(Integer.toString(Reverse.reverseDigit(Integer.parseInt(txtName.getText()))));
		//txtName.setText("HELLO " + txtName.getText());
	}
	
	public static void main(String[] args) {
		new ButtonTest();
	}

}
