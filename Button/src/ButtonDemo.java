import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class ButtonDemo extends JFrame implements ActionListener {

	private JButton btnClick;
	private JLabel lblHello;
	private JPanel panel1,panel2;
	public ButtonDemo() {
		panel1 = new JPanel();
		panel1.setBackground(Color.BLUE);
	
		panel2 = new JPanel();
		panel2.setBackground(Color.CYAN);
		
		lblHello = new JLabel("TEST");
		panel2.add(lblHello);
		
		panel1.add(panel2);
		btnClick = new JButton("CLICK ME!");
		add(panel1, BorderLayout.CENTER);
		add(btnClick,BorderLayout.SOUTH);
		btnClick.addActionListener(this);
		
		setSize(800,800);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		new ButtonDemo();
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(null, "Hello");
	}

}
